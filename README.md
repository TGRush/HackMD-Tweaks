# HackMD Tweaks
## Various little changes for HackMD

### Currently available Tweaks:

- More Colorful Buttons
- Disable Comments
- Editor Font Changer
- Fixes (currently only mobile spacing)

[Install with Stylus](https://codeberg.org/TGRush/HackMD-Tweaks/raw/branch/master/hackmd-tweaks.user.css)
